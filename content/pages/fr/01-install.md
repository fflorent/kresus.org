Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Installation
toc_run: true
toc_title: Comment installer Kresus&nbsp;?
lang: fr


## Installation avec Docker

### Lancez une image pré-construite

L'image Docker a l'avantage d'inclure toutes les dépendances nécessaires, dont
une installation de Weboob complète. A chaque redémarrage de l'image, Weboob
essayera de se mettre à jour ; si vous rencontrez donc un problème de modules,
il est recommandé de simplement redémarrer l'image (un simple `restart`
suffit).

L'image pré-construite expose deux volumes de données. Il n'est pas obligatoire
de les monter, mais cela vous permet de conserver vos données personnelles
entre des redémarrages de l'image Docker.

- `/home/user/data` contient toutes les données utilisées par Kresus. Il est
  recommandé de monter ce volume, pour éviter des pertes de données entre
  chaque nouveau démarrage de l'image (par exemple, après une mise à jour).
- `/weboob` contient le clone local de Weboob installé au démarrage. L'exposer
  au système local n'est pas obligatoire, mais permet de :
    - mutualiser les répertoires d'installation de Weboob, si plusieurs images
      Kresus sont instanciées. Cela permet notamment des économies d'espace
      disque.
    - mettre en place des `crontab` sur la machine hôte, qui se chargeront de
      mettre à jour Weboob régulièrement (avec un `git pull` depuis le
      répertoire de Weboob sur l'hôte).

**Note** : si vous clonez Weboob, il vous est suggéré d'utiliser [la version de devel](https://git.weboob.org/weboob/devel/) qui devrait être à jour.

La variable d'environnement suivante peut être définie :

- `LOCAL_USER_ID` permet de choisir l'UID de l'utilisateur interne de l'image
  Docker (afin de ne pas faire que kresus tourne en root dans l'image).

Voici un exemple de ligne de commande pour lancer Kresus en production dans une
image Docker, avec le même utilisateur UNIX que l'actuel :

    :::bash
    mkdir -p /opt/kresus/data
    mkdir -p /opt/kresus/weboob
    git clone https://git.weboob.org/weboob/devel.git /opt/kresus/weboob

    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        --name kresus \
        -ti -d bnjbvr/kresus

### Construire l'image soi-même

#### Image stable

L'image correspondant à la dernière version stable de Kresus peut être
téléchargée via le *hub* de Docker. Il s'agit de l'image accessible via
`bnjbvr/kresus`.

Il est possible de la reconstruire à la main. Vous aurez besoin de `nodejs` 4
ou plus récent et de `npm` pour reconstruire l'image de zéro.

    :::bash
    git clone https://framagit.org/bnjbvr/kresus && cd kresus
    docker build -t myself/kresus -f docker/Dockerfile-stable .

Vous pouvez ensuite l'utiliser:

    :::bash
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d myself/kresus


#### Image _Nightly_

Il existe aussi une image _Nightly_, avec les derniers changements, construite
chaque nuit à partir du dépôt Git. Attention, cette image est expérimentale et
peut contenir de nombreux bugs ou corrompre vos données, car la branche
_master_ peut être instable de temps en temps. Cette image récupèrera les
dernières sources depuis le dépôt Git en ligne et n'utilisera donc pas de
sources locales.

Pour constuire une image _Nightly_ en version de développement (pas de minification, mieux pour débugger mais inadapté à un usage en production):

    :::bash
    make docker-nightly-dev


Pour construire une image _Nightly_ compilée pour une utilisation en
production:

    :::bash
    make docker-nightly-prod

Ces commandes construiront une image nommée `bnjbvr/kresus-nightly-dev`
(respectivement `bnjbvr/kresus-nightly-prod`), ainsi que l'image de base
commune à ces deux environnements.

Pour la lancer ensuite, vous pouvez utiliser la même commande `docker run` que
précédemment, en adaptant le nom de l'image:

    :::bash
    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        --name kresus \
        -ti -d bnjbvr/kresus-nightly-prod


## Installation dans Yunohost

Une [application](https://github.com/LowMem/kresus_ynh) pour
[Yunohost](http://yunohost.org/) est disponible. Celle-ci est encore récente
et expérimentale, n'hésitez pas à nous signaler tout problème que vous
pourriez rencontrer.


## Pré-requis pour les autres installations

Kresus utilise [Weboob](http://weboob.org/) sous le capot, pour se connecter au
site web de votre banque. Vous aurez besoin d'[installer le cœur et les modules
Weboob](http://weboob.org/install) afin que l'utilisateur exécutant Kresus
puisse les utiliser.

Kresus nécessite la dernière version stable de Weboob. Bien que Kresus puisse
fonctionner avec de précédentes versions, les modules des banques peuvent être
obsolètes, et la synchronisation avec votre banque pourrait être
dysfonctionnelle.


## Installation autonome

**AVERTISSEMENT: Il n'y a aucun système d'authentification intégré dans Kresus,
il est donc risqué de l'utiliser tel quel. Choisissez cette option uniquement
si vous savez ce que vous faites et êtes capable de gérer l'authentification
vous-même…**

Cela installera les dépendances Node, construira le projet et installera le
programme dans le répertoire node.js global. Notez que si cet emplacement est
`/usr/local/bin`, vous devrez probablement lancer cette commande en tant que
root.

### Installation locale

Pour installer les dépendances node et compiler les scripts (cela
n'installera pas Kresus globalement) pour un usage en production :

    :::bash
    npm install && npm run build:prod

Vous pourrez alors lancer Kresus en utilisant

    :::bash
    NODE_ENV=production npm run start

_Note :_ Vous pouvez aussi utiliser `npm run build:dev` pour compiler les
scripts en mode de développement (mieux pour débugger mais inadapté pour un
usage en production).


## Installation globale

Autrement, si vous souhaitez installer Kresus globalement vous utiliserez :

    :::bash
    make install

Et pourrez ensuite simplement lancer Kresus depuis n'importe quel terminal
depuis n'importe quel répertoire avec :

    :::bash
    kresus


## Installation sur CozyCloud

Si vous possédez déjà une instance Cozy, la meilleure solution (et la
[seule](https://github.com/cozy/cozy-home/issues/789)) est d'installer Kresus
depuis le *marché*.


## Configuration

### Avec un fichier `config.ini`

Vous pouvez définir toutes les options à utiliser dans un fichier `INI` et
passer l'argument `-c path/to/config.ini` à Kresus au lancement. Un fichier
`config.ini.example` est disponible dans le dépôt Git pour lister les options
disponibles. Il peut être copié et édité pour mieux correspondre à vos choix.

**Sécurité :** En mode production (`NODE_ENV=production`), si le fichier de
configuration ne fournit pas uniquement les droits de lecture ou
lecture/écriture à son propriétaire, en utilisant les permissions du système
de fichier, Kresus refusera de démarrer.


### Avec des variables d'environnement

Notez que chaque option de configuration a une variable d'environnement
associée : si la variable d'environnement est définie, elle surchargera la
configuration du fichier `INI` ou la valeur par défaut. Référez-vous au
fichier `config.ini.example` pour trouver toutes les variables d'environnement
utilisables.


## Recommandations pour le pare-feu

Vous devrez définir les autorisations suivantes :

- Accès http/https au site web de votre banque, pour récupérer les nouvelles
  opérations.
- Accès http/https aux dépôts Weboob, pour la mise à jour automatique des
  modules avant les rappatriements automatiques, si vous utilisez la
  fonctionnalité de mise à jour automatique.
