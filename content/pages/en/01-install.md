Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Installation
toc_run: true
toc_title: How to install Kresus&nbsp;?
lang: en


## Install with Docker

### Run a pre-built image

The Docker images already bundles all the required dependencies, including the
full Weboob install. At each boot, Weboob will try to update; if you have any
problem with modules you should then try to simply `restart` the image.

This pre-built image exposes two data volumes. It is not mandatory to mount
them, but it would let you persist your personal data across reboot of the
Docker image.

- `/home/user/data` stores all data used by Kresus. It is recommended to mount
  this volume, to avoid losing data at each restart of the image (for instance
  after an update).
- `/weboob` stores the local Weboob clone, set up at startup. It is not
  mandatory to expose it to the local filesystem, but this would allow you to
    - share Weboob installation folders across scripts and Kresus instances,
      resulting in less disk space being used.
    - set some `crontab` on the host machine to regularly update Weboob
      completely (with a `git pull` in the Weboob folder from the host).

**Note**: if you clone Weboob, it is suggested to use the [devel version](https://git.weboob.org/weboob/devel/) which is more likely to be up to date.

The following environment variable can be used:

- `LOCAL_USER_ID` selects the UID of the user inside the Docker image (for
  preventing Kresus from running as root).

Here is an example of command line to run to start Kresus in production in a
Docker image, with the same UNIX user as current:

    :::bash
    mkdir -p /opt/kresus/data
    mkdir -p /opt/kresus/weboob
    git clone https://git.weboob.org/weboob/devel.git /opt/kresus/weboob

    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        --name kresus \
        -ti -d bnjbvr/kresus

### Build the Docker image

#### Stable image

The image for the latest stable version of Kresus can be downloaded through
the Docker hub. This is the image called `bnjbvr/kresus`.

You can also build it yourself. You will need `nodejs` 4 or later and `npm` to
build the image from scratch.

    :::bash
    git clone https://framagit.org/bnjbvr/kresus && cd kresus
    docker build -t myself/kresus -f docker/Dockerfile-stable .

You can then use it:

    :::bash
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d myself/kresus


#### Nightly image

There is also a nightly image, with the latest updates, which is built every
night from the Git repository. Be warned that this image is experimental and can
contain a lot of bugs or corrupt your data, as the `master` branch can be
instable from time to time. This image will fetch the latest sources from the
online Git repository and will not use any local sources.

To build a Nightly image in development mode (no minification, better for
debugging but not meant to be used in production):

    :::bash
    make docker-nightly-dev


To build a Nightly image built for a use in production:

    :::bash
    make docker-nightly-prod

These commands will build an image named `bnjbvr/kresus-nightly-dev`
(respectively `bnjbvr/kresus-nightly-prod`), as well as the base image common
to these two environments.

Then, to start it, you can use the same `docker run` command as before, with
the correct image name:

    :::bash
    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        --name kresus \
        -ti -d bnjbvr/kresus-nightly-prod


## Installation within Yunohost

A [Yunohost](http://yunohost.org/) [app](https://github.com/LowMem/kresus_ynh)
is available. It is still new and experimental, please report us any problem
you might encounter with it.


## Requirements for other install methods

Kresus makes us of [Weboob](http://weboob.org/) under the hood, to contact
your bank website. You will have to [install Weboob core and
modules](http://weboob.org/install) so that the user running Kresus can use
them.

Kresus requires the latest stable version of Weboob. Although Kresus could
work with previous versions, bank modules can get obsolete and the
synchronisation with your bank might not work with older versions.


## Standalone install

**AVERTISSEMENT: There is no builtin authentication mechanism in Kresus, it is
then risky to use it as is. Choose this option only if you know what you are
doing and are able to handle authentication yourself.**

The following will install remaining dependencies, build the project and
install Kresus in the global Node folder. Note that if this folder is
`/usr/local/bin`, you will likely have to run the command as root.

### Local install

To install Node dependencies and build the scripts (this will not install
Kresus globally) for a production use:

    :::bash
    npm install && npm run build:prod

You can then run Kresus using:

    :::bash
    NODE_ENV=production npm run start

_Note :_ You can also use `npm run build:dev` to build the scripts in
development mode (better for debugging, but should not be used in production).


## Global install

Alternatively, if you want to install Kresus globally, you can use:

    :::bash
    make install

You will then be able to start Kresus from any terminal from any folder using:

    :::bash
    kresus


## CozyCloud install

If you already have a Cozy instance, the best (and
[only](https://github.com/cozy/cozy-home/issues/789)) solution is to install
Kresus from the *market*.


## Configuration

### Using a `config.ini` file

You can set the options to use in a `INI` file and pass the command-line
argument `-c path/to/config.ini` to Kresus. A `config.ini.example` file is
available in the Git repository to list all available options. It can be
copied and edited to better match your choices.

**Security :** In production mode (`NODE_ENV=production`), the configuration
folder should provide only read or read/write rights to its owner, using the
filesystem permissions. Otherwise, Kresus will not start.


### Using environment variables

Note that every configuration option has a matching environment variable: if
the environment variable is set, it will overload the configuration from the
`INI` file or the default value. Please refer to the `config.ini.exmple` file
to find the available environment variables.


## Firewall recommendations

You'll need the following firewall authorizations:

* http/https access to your bank website, for fetching new operations on your
behalf.
* http/https access to the Weboob repositories, for automatically updating the
bank modules before automatic pollings.
